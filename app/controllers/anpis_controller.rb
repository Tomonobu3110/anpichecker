class AnpisController < ApplicationController
  def index
    @anpi_strings = ["", "1. 無事", "2. 被害あり", "3. 不在", "4. その他"]
    #@anpis = Anpi.group('room').having('created_at = MAX(created_at)')
    # see) http://labs.timedia.co.jp/2014/10/selecting-max-record-in-group-by.html
    @anpis = Anpi.find_by_sql(['select * from anpis as m where not exists (select 1 from anpis as s where m.room = s.room and m.created_at < s.created_at) order by room;'])
  end

  def all
    @anpi_strings = ["", "1. 無事", "2. 被害あり", "3. 不在", "4. その他"]
    @anpis = Anpi.all
  end
  
  def show
  end

  def new
    @anpi = Anpi.new
  end

  def create
    @anpi = Anpi.new(
      room: params[:room],
      status: params[:status]
    )
    if @anpi.save
      flash[:notice] = "安否情報を登録しました"
      redirect_to("/anpis/index")
    else
      render("/anpis/new")
    end
  end
end
