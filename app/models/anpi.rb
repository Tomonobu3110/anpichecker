class Anpi < ApplicationRecord
  validates :room, presence: true, format: {with: /\A[1-7][0-1][0-9]\z/}
end
