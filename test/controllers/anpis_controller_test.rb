require 'test_helper'

class AnpisControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get anpis_index_url
    assert_response :success
  end

  test "should get show" do
    get anpis_show_url
    assert_response :success
  end

  test "should get new" do
    get anpis_new_url
    assert_response :success
  end

end
