class CreateAnpis < ActiveRecord::Migration[5.0]
  def change
    create_table :anpis do |t|
      t.string :room
      t.integer :status

      t.timestamps
    end
  end
end
