Rails.application.routes.draw do
  get 'anpis/index'
  get 'anpis/show'
  get 'anpis/new'
  get 'anpis/all'
  post 'anpis/create' => "anpis#create"

  get 'home/top'

  get "/" => "anpis#new"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
